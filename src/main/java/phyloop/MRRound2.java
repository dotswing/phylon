package phyloop;

import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;
import java.util.Iterator;

public class MRRound2 {

	public static class M extends MapReduceBase implements Mapper<Text, Text, Text, Text> {

		@Override
		public void map(Text key, Text value, OutputCollector<Text, Text> out, Reporter reporter) throws IOException {
			out.collect(key, value);
		}
	}

	public static class R extends MapReduceBase implements Reducer<Text, Text, Text, NullWritable> {

		String r2mDir;
		FileSystem fs;

		@Override
		public void configure(JobConf job) {
			r2mDir = job.get("phyloop.r2m.dir");
			try {
				fs = FileSystem.get(job);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		@Override
		public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, NullWritable> out, Reporter reporter) throws IOException {
			FSDataOutputStream stream = fs.create(new Path(r2mDir + "/"+key.toString()));

			while(values.hasNext()) {
				String[] value = values.next().toString().split("\\s");

				stream.writeLong(Long.valueOf(value[0].trim()));
				stream.writeDouble(Double.valueOf(value[1].trim()));
			}

			stream.close();

		}
	}

}
