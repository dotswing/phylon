package phyloop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.mapred.lib.NLineInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;


public class Phyloop extends Configured implements Tool {

	String outdir;
	String datadir;
	int numMapper = 5;
	int numReducer = 5;
	String r2mdir;
	String r3input;

	public  void round1() throws IOException {
		JobConf jobConf = new JobConf(getConf(), getClass());

		jobConf.setInputFormat(PInputFormat.class);
		jobConf.setOutputFormat(TextOutputFormat.class);

		jobConf.setMapOutputValueClass(LongWritable.class);

		jobConf.setOutputKeyClass(Text.class);
		jobConf.setOutputValueClass(Text.class);

		FileInputFormat.setInputPaths(jobConf, datadir);
		FileOutputFormat.setOutputPath(jobConf, new Path(outdir+"/r1"));

		jobConf.setMapperClass(PMapper.class);
		jobConf.setReducerClass(PReducer.class);

		jobConf.setNumMapTasks(numMapper);
		jobConf.setNumReduceTasks(numReducer);

		JobClient.runJob(jobConf);
	}

	public void round2() throws IOException {
		JobConf jobConf = new JobConf(getConf(), getClass());
		jobConf.set("key.value.separator.in.input.line", "@");
		jobConf.set("phyloop.r2m.dir", r2mdir);

		FileSystem.get(jobConf).mkdirs(new Path(r2mdir));

		jobConf.setInputFormat(KeyValueTextInputFormat.class);
		jobConf.setOutputFormat(TextOutputFormat.class);

		jobConf.setMapOutputValueClass(Text.class);

		jobConf.setOutputKeyClass(Text.class);
		jobConf.setOutputValueClass(NullWritable.class);

		FileInputFormat.setInputPaths(jobConf, new Path(outdir+"/r1"));
		FileOutputFormat.setOutputPath(jobConf, new Path(outdir+"/r2"));

		jobConf.setMapperClass(MRRound2.M.class);
		jobConf.setReducerClass(MRRound2.R.class);

		jobConf.setNumMapTasks(1);
		jobConf.setNumReduceTasks(numReducer);

		JobClient.runJob(jobConf);
	}

	public void round3() throws IOException {
		JobConf jobConf = new JobConf(getConf(), getClass());

		jobConf.set("phyloop.r2m.dir", r2mdir);

		jobConf.setInputFormat(NLineInputFormat.class);
		jobConf.setOutputFormat(TextOutputFormat.class);

		jobConf.setOutputKeyClass(Text.class);
		jobConf.setOutputValueClass(Text.class);

		FileInputFormat.setInputPaths(jobConf, new Path(r3input));
		FileOutputFormat.setOutputPath(jobConf, new Path(outdir+"/r3"));

		jobConf.setMapperClass(MRRound3.M.class);
		jobConf.setReducerClass(MRRound3.R.class);

		jobConf.setNumMapTasks(numMapper);
		jobConf.setNumReduceTasks(1);

		JobClient.runJob(jobConf);
	}

	public void prepare() throws IOException {
		JobConf jobConf = new JobConf(getConf(), getClass());
		FileSystem fs = FileSystem.get(jobConf);
		FSDataOutputStream out = fs.create(new Path(r3input));
		FileStatus[] s = fs.listStatus(new Path(datadir));

		for (int i=0;i<s.length;i++) {
			FileStatus a = s[i];
			for (int j=+1;j<s.length;j++) {
				FileStatus b = s[j];
				out.writeChars(Utils.basename(a.getPath().getName()));
				out.writeChars(".");
				out.writeChars(Utils.basename(b.getPath().getName()));
				out.writeChar('\n');
			}
		}

	}

	@Override
	public int run(String[] args) throws Exception {
		datadir = args[0];
		numMapper = Integer.valueOf(args[1]);
		numReducer = Integer.valueOf(args[2]);
		outdir = args[3];
		r2mdir = outdir+"/r2m";
		r3input = outdir+"/r3input";

		prepare();
		//round1();
		//round2();
		round3();

		return 0;
	}

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new Configuration(), new Phyloop(), args));
	}

}
