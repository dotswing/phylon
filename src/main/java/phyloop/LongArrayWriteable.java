package phyloop;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.LongWritable;

public class LongArrayWriteable extends ArrayWritable {
	public LongArrayWriteable() {
		super(LongWritable.class);
	}
}
