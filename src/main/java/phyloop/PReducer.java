package phyloop;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;
import java.util.Iterator;

public class PReducer extends MapReduceBase implements Reducer<Text, LongWritable, Text, Text> {
	@Override
	public void reduce(Text key, Iterator<LongWritable> values,
					   OutputCollector<Text, Text> out, Reporter reporter)
			throws IOException {
		long sum = 0;
		while (values.hasNext()) {
			sum += values.next().get();
		}

		double kmer_size = 0.0;
		switch (key.toString().split("\\.")[1].charAt(0)) {
			case '1':
				kmer_size = Utils.KMER_SIZE_5;
				break;
			case '2':
				kmer_size = Utils.KMER_SIZE_1;
				break;
			case 'k':
				kmer_size = Utils.KMER_SIZE_6;
				break;
		}

		out.collect(key, new Text(String.valueOf(sum / kmer_size)));
	}
}
