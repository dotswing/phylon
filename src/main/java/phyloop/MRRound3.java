package phyloop;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MRRound3 {

	public static class M extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

		FileSystem fs;
		String r2mdir;

		@Override
		public void configure(JobConf job) {
			//r2mdir = job.get("phyloop.r2m.dir");
			r2mdir = "b52/r2m";
			try {
				fs = FileSystem.get(job);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void map(LongWritable key, Text value, OutputCollector<Text, Text> out, Reporter reporter) throws IOException {
			StringBuffer buf = new StringBuffer();
			for (char c : value.toString().toCharArray()) {
				if (c != 0) {
					buf.append(c);
				}
			}

			String s[] = buf.toString().split("\\.");
			if (s.length != 2) {
				return;
			}

			String a = s[0];
			String b = s[1];

			Map<Long, Double> ma1 = restoreArray(a, "1");
			Map<Long, Double> ma2 = restoreArray(a, "2");
			Map<Long, Double> mak = restoreArray(a, "k");
			Map<Long, Double> maf = feq(ma1, ma2, mak);

			Map<Long, Double> mb1 = restoreArray(b, "1");
			Map<Long, Double> mb2 = restoreArray(b, "2");
			Map<Long, Double> mbk = restoreArray(b, "k");
			Map<Long, Double> mbf = feq(mb1, mb2, mbk);

			double d = distance_cos(maf, mbf);

			out.collect(value, new Text(String.valueOf(d)));
		}

		public Map<Long, Double> restoreArray(String name, String anno) throws IOException {
			String filename = name + "." + anno;

			FSDataInputStream stream = fs.open(new Path(r2mdir+"/"+filename));
			Map<Long, Double> rmap = new HashMap<Long, Double>();

			try {
				long pos = stream.readLong();
				double value = stream.readDouble();
				rmap.put(pos, value);

			} catch (Exception e) {
			}

			stream.close();
			return rmap;
		}

		public Map<Long, Double> feq(Map<Long, Double> m1, Map<Long, Double> m2, Map<Long, Double> mk) {
			Map<Long, Double> mf = new HashMap<Long, Double>();

			for (Long pos : mk.keySet()) {
				Double value = mk.get(pos);
				double f = value * 1.0;
				String kmer = Utils.i2s(pos, 6);

				double f_L = get(m2, Utils.s2i(kmer.substring(0, 1)));
				double f_R = get(m2, Utils.s2i(kmer.substring(5, 6)));

				double f_Lw = get(m1, Utils.s2i(kmer.substring(0, 5)));
				double f_wR = get(m1, Utils.s2i(kmer.substring(1, 6)));

				double q = (f_R * f_Lw + f_L * f_wR) / 2;

				mf.put(pos, (f -q) /q);

			}

			return mf;
		}

		private double get(Map<Long, Double> m, Long i) {
			if (m.containsKey(i)) {
				return m.get(i);
			} else {
				return 0.0;
			}
		}


		public double distance_cos(Map<Long, Double> maf, Map<Long, Double> mbf) {
			double ab= 0.0;
			double a = 0.0;
			double b = 0.0;

			for (int i=0;i<Utils.KMER_SIZE_6;i++) {
				double fa = get(maf, (long)i);
				double fb = get(mbf, (long)i);
				ab = ab + fa * fb;
				a = a + fa * fa;
				b = b + fb * fb;
			}
			a = Math.sqrt(a);
			b = Math.sqrt(b);

			double cosine = ab / (a*b);

			return 0.5 * (1 - cosine);
		}

	}


	public static class R extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

		@Override
		public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> out, Reporter reporter) throws IOException {
			out.collect(key, values.next());
		}
	}

}
