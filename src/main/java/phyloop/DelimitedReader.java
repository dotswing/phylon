package phyloop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;

import java.io.IOException;
import java.io.InputStream;

public class DelimitedReader {
	private static final int DEFAULT_BUFFER_SIZE = 64 * 1024;
	private int bufferSize = DEFAULT_BUFFER_SIZE;
	private InputStream in;
	private byte[] buffer;
	// the number of bytes of real data in the buffer
	private int bufferLength = 0;
	// the current position in the buffer
	private int bufferPosn = 0;

	// The line delimiter
	private int prefixLength;
	private int delimiterLength;
	private int suffixLength;
	private final byte[] del;

	/**
	 * Create a line reader that reads from the given stream using the
	 * <code>io.file.buffer.size</code> specified in the given
	 * <code>Configuration</code>, and using a custom delimiter of array of
	 * bytes.
	 * 
	 * @param in
	 *            input stream
	 * @param conf
	 *            configuration
	 * @param delimiterBytes
	 *            The delimiter
	 * @throws IOException
	 */
	public DelimitedReader(InputStream in, Configuration conf,
			byte[] prefixBytes, byte[] delimiterBytes, byte[] suffixBytes)
			throws IOException {
		this.in = in;
		this.bufferSize = conf.getInt("io.file.buffer.size",
				DEFAULT_BUFFER_SIZE);
		this.buffer = new byte[this.bufferSize];

		this.prefixLength = prefixBytes.length;
		this.delimiterLength = delimiterBytes.length;
		this.suffixLength = suffixBytes.length;
		this.del = new byte[prefixBytes.length + delimiterBytes.length
				+ suffixBytes.length];
		System.arraycopy(prefixBytes, 0, del, 0, prefixLength);
		System.arraycopy(delimiterBytes, 0, del, prefixLength, delimiterLength);
		System.arraycopy(suffixBytes, 0, del, prefixLength + delimiterLength,
				suffixLength);
	}

	/**
	 * Close the underlying stream.
	 * 
	 * @throws IOException
	 */
	public void close() throws IOException {
		in.close();
	}

	public int readLine(Text str, int maxLineLength, int maxBytesToConsume)
			throws IOException {
		str.clear();
		int txtLength = 0; // tracks str.getLength(), as an optimization
		long bytesConsumed = 0;
		int delPosn = 0;
		// int firstDel = suffixLength;
		do {
			int startPosn = bufferPosn;
			if (bufferPosn >= bufferLength) {
				startPosn = bufferPosn = 0;
				bufferLength = in.read(buffer);
				if (bufferLength <= 0) {
					break; // EOF
				}
			}
			for (; bufferPosn < bufferLength; ++bufferPosn) {
				// if (firstDel > 0) {
				// // Jump over the suffix
				// firstDel--;
				// } else {
				if (buffer[bufferPosn] == del[delPosn]) {
					delPosn++;
					if (delPosn >= del.length) {
						// rewind the buffer pointer back to the
						// head of delimiter
						bufferPosn = bufferPosn - suffixLength + 1;
						break;
					}
				} else {
					if (bufferPosn < delPosn) {
						// false positive
						// append partial delimiter
						bytesConsumed += bufferPosn - startPosn;
						startPosn = bufferPosn;
						str.append(del, 0, delPosn);
					}
					delPosn = 0;
				}
			}
			// }
			int readLength = bufferPosn - startPosn;
			bytesConsumed += readLength;
			// here we don't append a prefix of delimiter
			int appendLength = readLength - delPosn;
			if (delPosn == del.length) {
				// because bufferPos has been rewinded
				appendLength += suffixLength + prefixLength;
			}
			if (appendLength > maxLineLength - txtLength) {
				appendLength = maxLineLength - txtLength;
			}
			if (appendLength > 0) {
				str.append(buffer, startPosn, appendLength);
				txtLength += appendLength;
			}
		} while (delPosn < del.length && bytesConsumed < maxBytesToConsume);
		if (bytesConsumed > (long) Integer.MAX_VALUE)
			throw new IOException("Too many bytes before delimiter: "
					+ bytesConsumed);
		if (bytesConsumed < maxBytesToConsume) {
			if (delPosn < del.length) {
				// EOF encountered, append partial delimiter
				// but alow file end with prefix+EOF or prefix+delim+EOF
				if (delPosn == prefixLength + delimiterLength) {
					// remove appending delimiter
					str.append(del, 0, prefixLength);
				} else {
					str.append(del, 0, delPosn);
				}
			} else {
				// delimiter encountered, append prefix
				str.append(del, 0, prefixLength);
			}
		}
		return (int) bytesConsumed;
	}

	public int readLine(Text str) throws IOException {
		return readLine(str, Integer.MAX_VALUE, Integer.MAX_VALUE);
	}

}
