package phyloop;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.mapred.*;

import java.io.IOException;

public class PInputFormat extends FileInputFormat implements JobConfigurable {

	private byte[] suffix = ">".getBytes();
	private byte[] prefix;
	private byte[] delim = "\n".getBytes();

	private CompressionCodecFactory compressionCodecs = null;

	public void configure(JobConf conf) {
		compressionCodecs = new CompressionCodecFactory(conf);
	}

	protected boolean isSplitable(FileSystem fs, Path file) {
		return compressionCodecs.getCodec(file) == null;
	}

	public RecordReader<Text, Text> getRecordReader(
			InputSplit genericSplit, JobConf job, Reporter reporter)
			throws IOException {
		reporter.setStatus(genericSplit.toString());
		return new DelimitedRecordReader(job, (FileSplit) genericSplit, prefix,
				delim, suffix);
	}

	public static class DelimitedRecordReader implements RecordReader<Text, Text> {

		private static final Log LOG = LogFactory
				.getLog(DelimitedRecordReader.class.getName());

		private CompressionCodecFactory compressionCodecs = null;
		private long start;
		private long pos;
		private long end;
		private DelimitedReader in;

		int maxLineLength = Integer.MAX_VALUE;

		Text key;

		public DelimitedRecordReader(Configuration job, FileSplit split,
									 byte[] prefixBytes, byte[] delimiterBytes, byte[] suffixBytes)
				throws IOException {
			start = split.getStart();
			end = start + split.getLength();
			final Path file = split.getPath();
			key = new Text(file.getName().substring(0, file.getName().lastIndexOf('.')));

			compressionCodecs = new CompressionCodecFactory(job);
			final CompressionCodec codec = compressionCodecs.getCodec(file);
			prefixBytes = prefixBytes == null ? new byte[0] : prefixBytes;
			delimiterBytes = delimiterBytes == null ? new byte[0] : delimiterBytes;
			suffixBytes = suffixBytes == null ? new byte[0] : suffixBytes;
			// open the file and seek to the start of the split
			FileSystem fs = file.getFileSystem(job);
			FSDataInputStream fileIn = fs.open(split.getPath());
			boolean skipFirstLine = false;
			if (codec != null) {
				in = new DelimitedReader(codec.createInputStream(fileIn), job,
						prefixBytes, delimiterBytes, suffixBytes);
				end = Long.MAX_VALUE;
			} else {
				if (start != 0) {
					skipFirstLine = true;
					start -= prefixBytes.length + delimiterBytes.length;
					fileIn.seek(start);
				}
				in = new DelimitedReader(fileIn, job, prefixBytes, delimiterBytes,
						suffixBytes);
			}
			if (skipFirstLine) { // skip first line and re-establish "start".
				start += in.readLine(new Text(), 0,
						(int) Math.min((long) Integer.MAX_VALUE, end - start));
			}
			this.pos = start;
		}

		public Text createKey() {
			return key;
		}

		public Text createValue() {
			return new Text();
		}

		/** Read a line. */
		public synchronized boolean next(Text key, Text value)
				throws IOException {

			int newSize = in.readLine(value, maxLineLength,
					Math.max((int) Math.min(Integer.MAX_VALUE, end - pos),
							maxLineLength));
			if (newSize == 0) {
				return false;
			}

			pos += newSize;
			return true;
		}

		/**
		 * Get the progress within the split
		 */
		public float getProgress() {
			if (start == end) {
				return 0.0f;
			} else {
				return Math.min(1.0f, (pos - start) / (float) (end - start));
			}
		}

		public synchronized long getPos() throws IOException {
			return pos;
		}

		public synchronized void close() throws IOException {
			if (in != null) {
				in.close();
			}
		}
	}

}
