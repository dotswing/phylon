package phyloop;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;

public class PMapper extends MapReduceBase implements Mapper<Text, Text, Text, LongWritable> {

	int K = 6;

	public static final LongWritable ONE = new LongWritable(1);


	@Override
	public void map(Text key, Text value, OutputCollector<Text, LongWritable> out, Reporter reporter) throws IOException {
		String seq = value.toString();
		seq = seq.substring(seq.indexOf("\n"));
		seq = seq.replaceAll("\\s", "");
		char[] cs = seq.toCharArray();

		for (int i=0;i<cs.length - K + 1;i++) {
			StringBuilder kmer = new StringBuilder();
			for (int j=0;j<K;j++) {
				kmer.append(cs[i+j]);
			}
			long index = Utils.s2i(kmer.toString());
			if (index < 0) {
				continue;
			}
			out.collect(new Text(key.toString()+".k@"+index), ONE);
		}

		for (int i=0;i<cs.length - K;i++) {
			StringBuffer kmer1 = new StringBuffer();
			for (int j=0;j<K-1;j++) {
				kmer1.append(cs[i+j]);
			}
			long index = Utils.s2i(kmer1.toString());
			if (index < 0) {
				continue;
			}
			out.collect(new Text(key.toString()+".1@"+index), ONE);
		}

		for (int i=0;i<cs.length;i++) {
			long index = Utils.s2i(String.valueOf(cs[i]));
			if (index < 0) {
				continue;
			}
			out.collect(new Text(key.toString()+".2@"+index), ONE);
		}
	}


}
