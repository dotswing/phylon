package phyloop;

import java.util.Arrays;

public class Utils {

	public static final char[] ALPHABET =
			new char[]{'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y'};

	public static final double KMER_SIZE_6 = Math.pow(ALPHABET.length, 6);
	public static final double KMER_SIZE_5 = Math.pow(ALPHABET.length, 5);
	public static final double KMER_SIZE_1 = Math.pow(ALPHABET.length, 1);

	public static Long s2i(String s) {
		long result = 0;
		int l_s = s.length();
		for (int i=0;i<l_s;i++) {
			char c = s.charAt(i);

			if (c == 'X' || c == 'U' || c == 'J') {
				c = 'A';
			}
			else if (c == 'B') {
				c = 'N';
			}
			else if (c == 'Z') {
				c = 'Q';
			}

			int index = Arrays.binarySearch(ALPHABET, c);
			if (index < 0) {
				return new Long(-1);
			}
			result += index * (Math.pow(ALPHABET.length, l_s - i -1));
		}
		return new Long(result);
	}

	public static String i2s(long i, int n) {
		StringBuffer buf = new StringBuffer();
		for (int j=0;j<n;j++) {
			long base = (long) Math.pow(ALPHABET.length, n -j + 1);
			buf.append(ALPHABET[(int) (i / base)]);
			if (i >= base) {
				i = i - base * (i / base);
			}

		}
		return buf.toString();
	}

	public static String basename(String name) {
		return name.substring(0, name.lastIndexOf('.'));
	}

}
